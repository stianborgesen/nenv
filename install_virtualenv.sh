#!/usr/bin/env bash
virtualenv -p /usr/bin/python2.7 .venv
source .venv/bin/activate
pip install nodeenv
nodeenv --node=10.15.2 --jobs=4 .nenv
source .nenv/bin/activate
